// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract Record {

    mapping (uint => bool) public records;

    function storeRecord(uint256 _recordHash) public {
        records[_recordHash] = true;
    }

    function checkRecord(uint256 _recordHash) public view returns (bool) {
        return records[_recordHash];
    }
    
}