// scripts/deploy.js

const { ethers } = require("hardhat");
const fs = require("fs");
const path = require("path");

const getTheAbi = (contract) => {
  try {
      const dir = path.resolve(__dirname, "../artifacts/contracts/" + contract + ".sol/" + contract + ".json");
      const file = fs.readFileSync(dir, "utf8");
      const json = JSON.parse(file);
      const abi = json.abi;
      return JSON.stringify(abi);
  } catch (e) {
      console.error("Error reading ABI:", e);
  }
};

async function main() {
  const [deployer] = await ethers.getSigners();
  console.log("Deploying contracts with the account: " + deployer.address);

  console.log("\x1b[36m%s\x1b[0m","=============================================");
  console.log("\x1b[36m%s\x1b[0m","       Deploying Contracts");
  console.log("\x1b[36m%s\x1b[0m","=============================================");

  const gasLimit = 5000000; // Example gas limit value

  // Deploy Record
  const Record = await ethers.getContractFactory("Record");
  const record = await Record.deploy({
    gasLimit: gasLimit
  });
  await record.waitForDeployment();
  console.log(
    `Record contract deployed to ${record.target.toLowerCase()}`
  );
  console.log("\x1b[36m%s\x1b[0m","Record ABI:");
  console.log(getTheAbi("Record"));

  
  // console.log("\x1b[36m%s\x1b[0m","Events Bytecode:")
  // console.log(Record.bytecode)

  console.log("\x1b[36m%s\x1b[0m","=============================================");

}

main()
  .then(() => process.exit())
  .catch(error => {
    console.error(error);
    process.exit(1);
});